package org.davinci.aspectouno;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Cadenas {

    @Test(dataProvider = "words")
    void writeStrings(String stringOne, String stringTwo){
        System.out.println(compareString(stringOne,stringTwo));
    }


    @DataProvider(name="words")
    Object[][] getDataFromDataProvider(){
        return new Object[][]{
                {"Gilberto", "México"},
                {"moto","mota"},
                {"casa", "casa"}
        };
    }

    public String compareString(String stringOne, String stringTwo) {
        int compare = stringOne.compareTo(stringTwo);
        if (compare == 0) {
            return "Los Strings son iguales";
        } else if (compare > 0) {
            return "El String " + stringOne + " es mayor que el String " + stringTwo;
        } else {
            return "El String " + stringOne + " es menor que el String " + stringTwo;
        }
    }

}
