package org.davinci.aspectodos;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Letters {
    @Test(dataProvider = "words")
    void getElementsFromString(String stringPhrase){
        int contador[] = count(stringPhrase);
        System.out.println("El texto: " + stringPhrase + " contiene");
        System.out.println(contador[0] + " vocales");
        System.out.println(contador[1] + " consonantes o símbolos");
        System.out.println(contador[2] + " números\n");
    }


    @DataProvider(name="words")
    Object[][] getDataFromDataProvider(){
        return new Object[][]{
                {"Ambos miramos al @bismo,"},
                {"p3r0 cuando el @bismo nos m1ró,"},
                {"tú parp@deast3"}
        };
    }

    public int[] count(String phrase) {
        int letter[] = new int[3];
        letter[0] = 0;
        letter[1] = 0;
        letter[2] = 0;

        for (int i = 0; i < phrase.length(); i++) {
            String element = Character.toString(phrase.charAt(i));
            if (element.matches("[aeiouAEIOU]*")) {
                letter[0] += 1;
            } else if (element.matches("[0-9]*")) {
                letter[2] += 1;
            } else {
                letter[1] += 1;
            }

        }
        return letter;
    }
}
